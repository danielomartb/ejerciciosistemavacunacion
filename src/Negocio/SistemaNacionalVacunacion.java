/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.*;
import java.time.LocalDate;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.varios.ArchivoLeerURL;

/**
 * Ejercicio del sistema de vacunación , el enunciado lo encuentran 
 * en classroom en su grupo respectivo. 
 * @author madarme
 */
public class SistemaNacionalVacunacion {
    
    
    private int etapa;
    private String urlDpto,urlMunicipio, urlPersona, urlContenedor;
    private final ListaCD<Departamento> dptos=new ListaCD();
    private final ListaCD<NotificacionVacunado> registros=new ListaCD();
    private Proveedor []proveedores;

    public SistemaNacionalVacunacion() {
    }

    public SistemaNacionalVacunacion(int etapa, String urlDpto, String urlMunicipio, String urlPersona, String urlContenedor) {
        this.etapa = etapa;
        this.urlDpto = urlDpto;
        this.urlMunicipio = urlMunicipio;
        this.urlPersona = urlPersona;
        this.urlContenedor = urlContenedor;
    }
    /**
     * Punto 1
     */
    public void cargarPersonas()
    {
        // :)
    }
    /**
     *  Punto 2
     */
    public void cargarDptos() {
        ArchivoLeerURL file = new ArchivoLeerURL(this.urlDpto);
        Object v[] = file.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String linea = v[i].toString();
            //linea="1;Antioquia"
            String datos[] = linea.split(";");
            //datos[0]=1 y datos[1]=Antioquia
            Departamento nuevo = new Departamento(Integer.parseInt(datos[0]), datos[1]);
            this.dptos.insertarAlFinal(nuevo);

        }
    }
    /**
     * Punto 3
     */
    public void cargarMunicipios()
    {
       if(this.dptos.esVacia())
           throw new RuntimeException("No se puede realizar el proceso de cargar municpios por que no hay dptos");
       
        ArchivoLeerURL file = new ArchivoLeerURL(this.urlMunicipio);
        Object v[] = file.leerArchivo();
        for(int i=1;i<v.length;i++)
        {
            String linea=v[i].toString();
            //formato: id_dpto;id_municipio;nombreMunicipio
            //linea="1;3;Abejorral"
            String datos[]=linea.split(";");
            //datos[0]=1 , datos[1]=3 y datos[2]=Abejorral
            Municipio nuevo=new Municipio(Integer.parseInt(datos[1]),datos[2]);
            Departamento dpto=this.getBuscar(Integer.parseInt(datos[0]));
            if(dpto==null)
                throw new RuntimeException("No puede crearse el municipio por que no existe el código del dpto");
            
            dpto.getMunicipios().insertarAlFinal(nuevo);
            
        }
    }
    
    
    private Departamento getBuscar(int id_dpto)
    {
        for(Departamento dato:this.dptos)
        {
            if(dato.getId_dpto()==id_dpto)
                return dato;
        }
        return null;
    }
    
    
    
    /**
     * Punto 4
     */
    public void cargarProveedores()
    {
    
    }
    
    /**
     * Esté método es el principal, es válido si y solo si, los archivos están
     * cargados en las estructura de datos respectivas.
     * Punto 5.
     * @param fecha_inicio 
     */
    public void crearNotificaciones(LocalDate fecha_inicio)
    {
        
        
    }
    
    /**
     * ESTÉ PUNTO ES VÁLIDO , SI Y SOLO SI, SE HA IMPLEMENTADO
     * LOS PUNTOS 1, 2,3 Y 4. 
     * Punto 5.b
     * @return una cadena con los municipios donde las personas no pudieron ser vacunadas
     */
    public String getMunicipiosPersonas_NO_Vacunadas()
    {
        return null;
    }
    
    /**
     *  Punto 5.c
     * @return una cadena con los nombres de los departamentos donde fueron enviadas más notificaciones
     */
    public String getDptos_Mas_Notificacion()
    {
        return null;
    }

    public int getEtapa() {
        return etapa;
    }

    public String getUrlDpto() {
        return urlDpto;
    }

    public String getUrlMunicipio() {
        return urlMunicipio;
    }

    public String getUrlPersona() {
        return urlPersona;
    }

    public String getUrlContenedor() {
        return urlContenedor;
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public ListaCD<NotificacionVacunado> getRegistros() {
        return registros;
    }

    public Proveedor[] getProveedores() {
        return proveedores;
    }

    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

    public void setUrlDpto(String urlDpto) {
        this.urlDpto = urlDpto;
    }

    public void setUrlMunicipio(String urlMunicipio) {
        this.urlMunicipio = urlMunicipio;
    }

    public void setUrlPersona(String urlPersona) {
        this.urlPersona = urlPersona;
    }

    public void setUrlContenedor(String urlContenedor) {
        this.urlContenedor = urlContenedor;
    }
    
    
    
    
    
    //Métodos auxiliares:
    
    
    public String getListado_Dpto()
    {
    
        return this.dptos.toString();
    }
    
    
}
